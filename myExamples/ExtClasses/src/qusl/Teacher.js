Ext.define('qusl.Teacher', {
    extend: 'qusl.Person',
    constructor: function(config) {
        this.initConfig(config);
    },
    config: {
        teacherId: null
    }
});